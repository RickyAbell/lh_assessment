describe('Checking for features', function() {

  afterEach(function() {
    // Clear out enabled features
    if (window.LH && window.LH.features) {
      window.LH.features = [];
    }
  });

  it('returns true for enabled features', function() {
    var LH = window.LH || [];
    LH.push('largeButtons');
    LH.push('sortByPrice');
    LH.push('showRecentlyViewed');

    expect(LH.isEnabled('largeButtons')).toBe(true);
    expect(LH.isEnabled('sortByPrice')).toBe(true);
    expect(LH.isEnabled('showRecentlyViewed')).toBe(true);
  });

  it('returns false for features not enabled', function() {
    var LH = window.LH || [];
    LH.push('largeButtons');

    expect(LH.isEnabled('sortByPrice')).toBe(false);
  });
});

describe('Enabled Ids', function() {

  afterEach(function() {
    if (window.LH && window.LH.features) {
      window.LH.features = [];
    }
  });

  it('returns ids for enabled features with an id', function() {
    var LH = window.LH || [];
    LH.push('sortByPrice');
    LH.push('showRecentlyViewed');

    var enabledIds = LH.getEnabledIDs();
    expect(enabledIds).toBe('AF');
  });

  it('does not return ids for enabled features without an id', function() {
    var LH = window.LH || [];
    LH.push('largeButtons');
    LH.push('sortByPrice');
    LH.push('showRecentlyViewed');

    var enabledIds = LH.getEnabledIDs();
    expect(enabledIds).toBe('AF');
  });

  it('returns an empty string when no features are enabled', function() {
    var LH = window.LH || [];

    var enabledIds = LH.getEnabledIDs();
    expect(enabledIds).toBe('');
  });
});