Love Holidays FE Assessment by Ricky Abell

# Problem Description
At loveholidays.com we run A/B tests on our website to see whether a feature change improves sales or harms sales. To do this we can update the site configuration on page load with the following:
 
//Site configuration 
var LH = window.LH || [];
LH.push('largeButtons');
LH.push('sortByPrice'); 
LH.push('showRecentlyViewed'); 
 
Within our code base we wrap feature changes in if/else statements:
 
if (LH.isEnabled('largeButtons')) { 
//use large buttons 
} else { 
//use original buttons 
} 
 
Some features (but not all) have a unique id assigned which gets added to API requests. The unique id is a single uppercase letter. 
 
From the example above ‘sortByPrice' is assigned ‘A’ and ‘showRecentlyViewed' is assigned ‘F’. The API request looks like: 
 
/api/example?features=AF 
 
LH has a function named getEnabledIDs that returns the string ‘AF’ if both sortByPrice and showRecentlyViewed are enabled. 
 
Please write and write unit tests for LH. 
 
The LH you write will be loaded asynchronously so the site configuration (above) may run before or after LH is declared. isEnabled and getEnabledIDs will always be called after LH is declared. 
 
Do not use any third party libraries except for unit tests. ES5 or ES6 (using Babel) are both okay. Please let us know how long you spend on this task. Good luck.

# Installation and running the solution
To run the unit tests, open SpecRunner.html which will then run and display the results of the unit tests.

# Notes on the solution
The A/B functionality is implemented in the file abTesting.js. It starts off by checking if the features were already pushed through as an array and then moves them to a property called features. Then it implements the functions isEnabled, getEnabledIDs and push and then puts the object back on the window object.
