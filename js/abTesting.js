(function (window) {
  var LH;

  // If the features were pushed before then LH is an array
  // otherwise we need to expose a push function to simulate the same interface
  if (window.LH) {
    if (Object.prototype.toString.call(window.LH) === '[object Array]') {
      LH = {
        features : window.LH
      }
    } else {
      LH = window.LH;
    }
  } else {
    LH = {
      features: []
    };
  }
  

  var featureIdMappings = {
    'sortByPrice': 'A',
    'showRecentlyViewed': 'F'
  };

  /**
  * Returns true if the id is enabled
  * @param {String} id The id to check
  */
  LH.isEnabled = function(id) {
    return (LH.features.indexOf(id) >= 0) ? true : false;
  }

  /**
  * Returns the IDs for enabled features
  */
  LH.getEnabledIDs = function() {
    var enabledIds = '';

    for (var i = 0; i < LH.features.length; i++) {
      var feature = LH.features[i];
      if (featureIdMappings[feature]) {
        enabledIds += featureIdMappings[feature];
      }
    }

    return enabledIds;
  }

  /**
  * Adds a new feature
  * @param {String} feature Adds the feature to the list of enabled features
  */
  LH.push = function(feature) {
    LH.features.push(feature);
  }

  window.LH = LH;

})(window);